import { Tag } from "./tags";

export interface Content {
  id: string;
  name: string;
  tags?: Tag[];
}

export interface Board extends Content {
  additional?: boolean;
}
export interface BoardConfig {
  main: string | null;
  additional: string[];
}

export type Race = Content;
export type Power = Content;
export interface Combo {
  race: Race;
  power: Power;
}

export interface Pack extends Content {
  category: string;
  bundle?: string;
  boards?: Board[];
  races?: Race[];
  powers?: Power[];
}
