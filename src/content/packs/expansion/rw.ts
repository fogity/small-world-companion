import { Pack } from "@/content";
import { Region, Restriction } from "@/content/tags";

const rw: Pack = {
  id: "sw-rw",
  name: "River World",
  category: "Expansions",
  boards: [
    {
      id: "river",
      name: "River World",
      tags: [
        Region.Cavern,
        Region.Coast,
        Region.Farmland,
        Region.Forrest,
        Region.Hill,
        Region.MagicSource,
        Region.Mine,
        Region.Mountain,
        Region.River,
        Region.Swamp,
        Restriction.NoFlying,
      ],
    },
  ],
};

export default rw;
