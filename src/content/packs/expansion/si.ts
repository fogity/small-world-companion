import { Pack } from "@/content";
import { Board, Region, Theme } from "@/content/tags";

const si: Pack = {
  id: "sw-si",
  name: "Sky Islands",
  category: "Expansions",
  boards: [
    {
      id: "sky",
      name: "Sky Islands",
      additional: true,
      tags: [
        Board.SkyIsland,
        Region.Cavern,
        Region.Farmland,
        Region.Forrest,
        Region.Hill,
        Region.MagicSource,
        Region.Mine,
        Region.Mountain,
        Region.Swamp,
      ],
    },
  ],
  races: [
    { id: "drakons", name: "Drakons" },
    { id: "escargots", name: "Escargots" },
    { id: "khans", name: "Khans", tags: [Region.Farmland, Region.Hill] },
    { id: "scarcrows", name: "Scarcrows" },
    { id: "scavengers", name: "Scavengers" },
    {
      id: "storm-giants",
      name: "Storm Giants",
      tags: [Region.Mountain, Theme.Storm],
    },
    { id: "wendigos", name: "Wendigos", tags: [Region.Forrest] },
  ],
  powers: [
    { id: "airborne", name: "Airborne" },
    { id: "exploring", name: "Exploring", tags: [Board.SkyIsland] },
    { id: "goldsmith", name: "Goldsmith", tags: [Region.Mine] },
    { id: "gunner", name: "Gunner" },
    { id: "haggling", name: "Haggling" },
    { id: "racketeering", name: "Racketeering" },
    { id: "zeppelined", name: "Zeppelined" },
  ],
};

export default si;
