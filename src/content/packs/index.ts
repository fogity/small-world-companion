import { preprocessPack } from "../util";
import sw from "./base/sw";
import swu from "./base/swu";
import sww from "./base/sww";
import rw from "./expansion/rw";
import si from "./expansion/si";
import asw from "./power/asw";
import bna from "./power/bna";
import c from "./power/c";
import gd from "./power/gd";
import rb from "./power/rb";

const packs = [asw, bna, c, gd, rb, rw, si, sw, swu, sww];

packs.forEach(preprocessPack);

export default packs;
