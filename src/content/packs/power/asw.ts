import { Pack } from "@/content";
import { Region } from "@/content/tags";

const asw: Pack = {
  id: "sw-asw",
  name: "A Spider's Web",
  category: "Expansions",
  bundle: "Power Pack 1",
  races: [
    { id: "ice-witches", name: "Ice Witches", tags: [Region.MagicSource] },
    { id: "skags", name: "Skags" },
    { id: "slingmen", name: "Slingmen" },
  ],
  powers: [
    { id: "copycat", name: "Copycat" },
    { id: "lava", name: "Lava", tags: [Region.Mountain] },
    { id: "soul-touch", name: "Soul-Touch" },
  ],
};

export default asw;
