import { Pack } from "@/content";

const bna: Pack = {
  id: "sw-bna",
  name: "Be Not Afraid...",
  category: "Expansions",
  bundle: "Power Pack 1",
  races: [
    { id: "barbarians", name: "Barbarians" },
    { id: "homunculi", name: "Homunculi" },
    { id: "leprechauns", name: "Leprechauns" },
    { id: "pixies", name: "Pixies" },
    { id: "pygmies", name: "Pygmies" },
  ],
  powers: [
    { id: "barricade", name: "Barricade" },
    { id: "catapult", name: "Catapult" },
    { id: "corrupt", name: "Corrupt" },
    { id: "imperial", name: "Imperial" },
    { id: "mercenary", name: "Mercenary" },
  ],
};

export default bna;
