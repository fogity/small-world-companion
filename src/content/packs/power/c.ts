import { Pack } from "@/content";

const c: Pack = {
  id: "sw-c",
  name: "Cursed!",
  category: "Expansions",
  bundle: "Power Pack 2",
  races: [
    { id: "goblins", name: "Goblins" },
    { id: "kobolds", name: "Kobolds" },
  ],
  powers: [
    { id: "cursed", name: "Cursed" },
    { id: "hordes-of", name: "Hordes of" },
    { id: "ransacking", name: "Ransacking" },
    { id: "were", name: "Were-" },
    { id: "marauding", name: "Marauding" },
  ],
};

export default c;
