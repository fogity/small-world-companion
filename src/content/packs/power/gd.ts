import { Pack } from "@/content";

const gd: Pack = {
  id: "sw-gd",
  name: "Grand Dames",
  category: "Expansions",
  bundle: "Power Pack 2",
  races: [
    { id: "gypsies", name: "Gypsies" },
    { id: "priestesses", name: "Priestesses" },
    { id: "white-ladies", name: "White Ladies" },
  ],
  powers: [
    { id: "historian", name: "Historian" },
    { id: "peace-loving", name: "Peace Loving" },
  ],
};

export default gd;
