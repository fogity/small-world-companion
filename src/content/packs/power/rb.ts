import { Pack } from "@/content";
import { Region, Theme } from "@/content/tags";

const rb: Pack = {
  id: "sw-rb",
  name: "Royal Bonus",
  category: "Expansions",
  bundle: "Power Pack 2",
  races: [
    { id: "fauns", name: "Fauns" },
    { id: "igors", name: "Igors" },
    { id: "shrubmen", name: "Shrubmen", tags: [Region.Forrest] },
  ],
  powers: [
    { id: "aquatic", name: "Aquatic", tags: [Region.Coast] },
    { id: "behemoth", name: "Behemoth", tags: [Region.Swamp] },
    {
      id: "fireball",
      name: "Fireball",
      tags: [Region.MagicSource, Theme.Scorched],
    },
  ],
};

export default rb;
