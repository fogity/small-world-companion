export enum Board {
  Island = "island",
  SkyIsland = "sky-island",
}

export enum Feature {
  RelicsAndPlaces = "relics-and-places",
}

export enum Power {
  SwapPower = "swap-power",
}

export enum Region {
  AbysmalChasm = "abysmal-chasm",
  Cavern = "cavern",
  Coast = "coast",
  Entry = "entry",
  Farmland = "farmland",
  Forrest = "forest",
  Hill = "hill",
  LakeOrSea = "lake-or-sea",
  MagicSource = "magic-source",
  Mine = "mine",
  Mountain = "mountain",
  River = "river",
  Swamp = "swamp",
  VolcanoZone = "volcano-zone",
}

export enum Restriction {
  NoFlying = "no-flying",
}

export enum Theme {
  Azeroth = "azeroth-theme",
  Recycled = "recycled-theme",
  Scorched = "scorched-theme",
  Storm = "storm-theme",
  Underground = "underground-theme",
}

export type Tag = Board | Feature | Power | Region | Restriction | Theme;
