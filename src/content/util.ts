import { includesValue } from "@/util";
import { Content, Pack, Power, Race } from ".";
import { Board, Feature, Region, Restriction, Tag, Theme } from "./tags";

export const dummyRace: Race = {
  id: "dummy-race",
  name: "Recycled",
  tags: [Theme.Recycled],
};

export const dummyPower: Power = {
  id: "dummy-power",
  name: "Recycled",
  tags: [Theme.Recycled],
};

export function preprocessPack(pack: Pack): void {
  if (!pack.tags) return;
  function appendTags<C extends Content>(content: C) {
    if (content.tags) content.tags.push(...(pack.tags ?? []));
    else content.tags = [...(pack.tags ?? [])];
  }
  if (pack.powers) pack.powers.forEach(appendTags);
  if (pack.races) pack.races.forEach(appendTags);
}

export function mergePacks(packs: Pack[]): Pack {
  return {
    id: "merged-content",
    name: "Merged Content",
    category: "Internal",
    boards: packs.flatMap((pack) => pack.boards ?? []),
    races: packs.flatMap((pack) => pack.races ?? []),
    powers: packs.flatMap((pack) => pack.powers ?? []),
  };
}

export interface FilterContext {
  boardTags?: Tag[];
  features?: Feature[];
}

export function filterContent<C extends Content>(
  items: C[],
  { boardTags, features }: FilterContext
): C[] {
  return items.filter(({ tags }) =>
    (tags ?? []).every((tag) => {
      switch (true) {
        case includesValue(Board, tag) || includesValue(Region, tag):
          return !boardTags || boardTags.includes(tag);
        case includesValue(Feature, tag):
          return !features || (features as Tag[]).includes(tag);
        case includesValue(Restriction, tag):
          return !boardTags || !boardTags.includes(tag);
      }
      return true;
    })
  );
}

export function compareContentName<C extends Content>(a: C, b: C): number {
  return a.name.localeCompare(b.name);
}
