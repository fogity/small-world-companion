import packs from "@/content/packs";
import { Feature } from "@/content/tags";
import {
  dummyPower,
  dummyRace,
  filterContent,
  FilterContext,
  mergePacks,
} from "@/content/util";
import { popRandom, remove, repeat } from "@/util";
import { computed, ComputedRef } from "vue";
import { createStore } from "vuex";
import {
  Actions,
  Getters,
  Mutations,
  PublicActions,
  PublicGetters,
  PublicState,
  State,
} from "./types";
import { ActionTree, GetterTree, MutationTree, Store } from "./util";

const state: State = {
  availablePacks: packs,
  enabledPacks: [],
  ignoreCompatibility: false,
  playerCount: 2,
  boardConfig: {
    main: null,
    additional: [],
  },
  unusedRaces: [],
  unusedPowers: [],
  availableCombos: [],
};

const getters: GetterTree<State, Getters> = {
  enabledContent(state) {
    return mergePacks(
      state.availablePacks.filter((pack) =>
        state.enabledPacks.includes(pack.id)
      )
    );
  },
  availableMainBoards(_state, getters) {
    return (getters.enabledContent.boards ?? []).filter(
      (board) => !board.additional
    );
  },
  availableAdditionalBoards(_state, getters) {
    return (getters.enabledContent.boards ?? []).filter(
      (board) => board.additional
    );
  },
  enabledBoardTags(state, getters) {
    const boardIds = [...state.boardConfig.additional];
    if (state.boardConfig.main) boardIds.push(state.boardConfig.main);
    const boards = (getters.enabledContent.boards ?? []).filter((board) =>
      boardIds.includes(board.id)
    );
    const tags = new Set(boards.flatMap((board) => board.tags ?? []));
    return [...tags.values()];
  },
  enabledFeatures(_state, getters) {
    const features: Feature[] = [];
    if (getters.enabledBoardTags.includes(Feature.RelicsAndPlaces))
      features.push(Feature.RelicsAndPlaces);
    return features;
  },
};

const mutations: MutationTree<State, Mutations> = {
  togglePack(state, { packId, enabled }) {
    if (enabled && !state.enabledPacks.includes(packId))
      state.enabledPacks.push(packId);
    else if (!enabled) remove(state.enabledPacks, packId);
  },
  toggleIgnoreCompatibility(state, { enabled }) {
    state.ignoreCompatibility = enabled;
  },
  setPlayerCount(state, { count }) {
    state.playerCount = count;
  },
  setMainBoard(state, { boardId }) {
    state.boardConfig.main = boardId;
  },
  toggleAdditionalBoard(state, { boardId, enabled }) {
    if (enabled && !state.boardConfig.additional.includes(boardId))
      state.boardConfig.additional.push(boardId);
    else if (!enabled) remove(state.boardConfig.additional, boardId);
  },
  resetGame(state, { unusedRaces, unusedPowers }) {
    state.unusedRaces = unusedRaces;
    state.unusedPowers = unusedPowers;
    state.availableCombos = [];
  },
  addCombo(state) {
    state.availableCombos.push({
      race: popRandom(state.unusedRaces) ?? dummyRace,
      power: popRandom(state.unusedPowers) ?? dummyPower,
    });
  },
  removeCombo(state, { row }) {
    state.availableCombos.splice(row, 1);
  },
  swapPower(state, { rows }) {
    const [rowA, rowB] = rows;
    const comboA = state.availableCombos[rowA];
    const comboB = state.availableCombos[rowB];
    if (!comboA || !comboB) return;
    const powerA = comboA.power;
    const powerB = comboB.power;
    comboA.power = powerB;
    comboB.power = powerA;
  },
};

const actions: ActionTree<State, Getters, Mutations, Actions> = {
  togglePack({ commit, dispatch }, payload) {
    commit("togglePack", payload);
    dispatch("adjustConfig");
  },
  toggleIgnoreCompatibility({ commit, dispatch }, payload) {
    commit("toggleIgnoreCompatibility", payload);
    dispatch("resetGame");
  },
  adjustConfig({ dispatch, state, getters }) {
    if (
      !getters.availableMainBoards.find(
        (board) => board.id === state.boardConfig.main
      )
    )
      dispatch("setMainBoard", {
        boardId: getters.availableMainBoards[0]?.id ?? null,
      });
    state.boardConfig.additional.forEach((boardId) => {
      if (
        !getters.availableAdditionalBoards.find((board) => board.id === boardId)
      )
        dispatch("toggleAdditionalBoard", { boardId, enabled: true });
    });
    dispatch("resetGame");
  },
  setPlayerCount({ commit, dispatch }, payload) {
    commit("setPlayerCount", payload);
    dispatch("resetGame");
  },
  setMainBoard({ commit, dispatch }, payload) {
    commit("setMainBoard", payload);
    dispatch("resetGame");
  },
  toggleAdditionalBoard({ commit, dispatch }, payload) {
    commit("toggleAdditionalBoard", payload);
    dispatch("resetGame");
  },
  resetGame({ commit, getters, state }) {
    if (state.ignoreCompatibility) {
      commit("resetGame", {
        unusedRaces: getters.enabledContent.races ?? [],
        unusedPowers: getters.enabledContent.powers ?? [],
      });
    } else {
      const context: FilterContext = {
        boardTags: getters.enabledBoardTags,
        features: getters.enabledFeatures,
      };
      const unusedRaces = filterContent(
        getters.enabledContent.races ?? [],
        context
      );
      const unusedPowers = filterContent(
        getters.enabledContent.powers ?? [],
        context
      );
      commit("resetGame", { unusedRaces, unusedPowers });
    }
    repeat(6, () => commit("addCombo"));
  },
  pickCombo({ commit }, payload) {
    commit("removeCombo", payload);
    commit("addCombo");
  },
  swapPower({ commit }, payload) {
    commit("swapPower", payload);
  },
};

export const store: Store<State, Getters, Mutations, Actions> = createStore({
  state,
  getters,
  mutations,
  actions,
});

export default store;

type PublicStore = Store<PublicState, PublicGetters, null, PublicActions>;

export function useStore(): PublicStore {
  return store;
}

export function useState<R extends keyof PublicState>(
  mapper: R[]
): { [K in R]: ComputedRef<PublicState[K]> };
export function useState<R extends { [K in string]: keyof PublicState }>(
  mapper: R
): { [K in keyof R]: ComputedRef<PublicState[R[K]]> };
export function useState(
  mapper: unknown
): { [K in string]: ComputedRef<unknown> } {
  if (mapper instanceof Array)
    mapper = Object.fromEntries(mapper.map((key) => [key, key]));
  return Object.fromEntries(
    Object.entries(
      mapper as { [K in string]: keyof PublicState }
    ).map(([mapperKey, stateKey]) => [
      mapperKey,
      computed(() => store.state[stateKey]),
    ])
  );
}

export function useGetters<R extends keyof PublicGetters>(
  mapper: R[]
): { [K in R]: ComputedRef<PublicGetters[K]> };
export function useGetters<R extends { [K in string]: keyof PublicGetters }>(
  mapper: R
): { [K in keyof R]: ComputedRef<PublicGetters[R[K]]> };
export function useGetters(
  mapper: unknown
): { [K in string]: ComputedRef<unknown> } {
  if (mapper instanceof Array)
    mapper = Object.fromEntries(mapper.map((key) => [key, key]));
  return Object.fromEntries(
    Object.entries(
      mapper as { [K in string]: keyof PublicGetters }
    ).map(([mapperKey, getterKey]) => [
      mapperKey,
      computed(() => store.getters[getterKey]),
    ])
  );
}

export function useActions<R extends keyof PublicActions>(
  mapper: R[]
): { [K in R]: PublicActions[K] };
export function useActions<R extends { [K in string]: keyof PublicActions }>(
  mapper: R
): { [K in keyof R]: PublicActions[R[K]] };
export function useActions(
  mapper: unknown
): { [K in string]: (payload: unknown) => void } {
  if (mapper instanceof Array)
    mapper = Object.fromEntries(mapper.map((key) => [key, key]));
  return Object.fromEntries(
    Object.entries(
      mapper as { [K in string]: keyof PublicState }
    ).map(([mapperKey, actionKey]) => [
      mapperKey,
      (payload) =>
        (store.dispatch as (type: string, payload: unknown) => void)(
          actionKey,
          payload
        ),
    ])
  );
}
