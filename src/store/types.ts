import { Pack, Combo, Race, Power, BoardConfig, Board } from "@/content";
import { Feature, Tag } from "@/content/tags";

export interface PublicState {
  // Content
  availablePacks: Pack[];
  enabledPacks: string[];
  ignoreCompatibility: boolean;
  // Players
  playerCount: number;
  // Board
  boardConfig: BoardConfig;
  // Combos
  availableCombos: Combo[];
}

export interface State extends PublicState {
  // Combos
  unusedRaces: Race[];
  unusedPowers: Power[];
}

export interface PublicGetters {
  // Board
  availableMainBoards: Board[];
  availableAdditionalBoards: Board[];
}

export interface Getters extends PublicGetters {
  // Content
  enabledContent: Pack;
  // Board
  enabledBoardTags: Tag[];
  // Features
  enabledFeatures: Feature[];
}

interface PublicMutationActions {
  // Content
  togglePack(payload: { packId: string; enabled: boolean }): void;
  toggleIgnoreCompatibility(payload: { enabled: boolean }): void;
  // Players
  setPlayerCount(payload: { count: number }): void;
  // Board
  setMainBoard(payload: { boardId: string | null }): void;
  toggleAdditionalBoard(payload: { boardId: string; enabled: boolean }): void;
  // Combos
  swapPower(payload: { rows: [number, number] }): void;
}

export interface Mutations extends PublicMutationActions {
  // Game
  resetGame(payload: { unusedRaces: Race[]; unusedPowers: Power[] }): void;
  // Combos
  addCombo(): void;
  removeCombo(payload: { row: number }): void;
}

export interface PublicActions extends PublicMutationActions {
  // Combos
  pickCombo(payload: { row: number }): void;
}

export interface Actions extends PublicActions {
  // Config
  adjustConfig(): void;
  // Game
  resetGame(): void;
}
