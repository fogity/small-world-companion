import { computed, ComputedRef } from "vue";
import {
  ActionContext as VuexActionContext,
  ActionTree as VuexActionTree,
  GetterTree as VuexGetterTree,
  MutationTree as VuexMutationTree,
  Store as VuexStore,
} from "vuex";

type Getters<G> = { getters: { [K in keyof G]: G[K] } };

type Commit<M> = {
  commit<T extends keyof M>(
    ...args: M[T] extends () => void
      ? [type: T]
      : M[T] extends (payload: infer P) => void
      ? [type: T, payload: P]
      : never
  ): void;
};

type Dispatch<A> = {
  dispatch<T extends keyof A>(
    ...args: A[T] extends () => void
      ? [type: T]
      : A[T] extends (payload: infer P) => void
      ? [type: T, payload: P]
      : never
  ): void;
};

type ActionContext<S, G, M, A> = Omit<
  VuexActionContext<S, S>,
  "getters" | "commit" | "dispatch"
> &
  Getters<G> &
  Commit<M> &
  Dispatch<A>;

export type GetterTree<S, G> = VuexGetterTree<S, S> &
  {
    [K in keyof G]: (state: S, getters: Omit<G, K>) => G[K];
  };

export type MutationTree<S, M> = VuexMutationTree<S> &
  {
    [K in keyof M]: M[K] extends () => void
      ? (state: S) => void
      : M[K] extends (payload: infer P) => void
      ? (state: S, payload: P) => void
      : never;
  };

export type ActionTree<S, G, M, A> = VuexActionTree<S, S> &
  {
    [K in keyof A]: A[K] extends () => void
      ? (context: ActionContext<S, G, M, A>) => void
      : A[K] extends (payload: infer P) => void
      ? (context: ActionContext<S, G, M, A>, payload: P) => void
      : never;
  };

export type Store<S, G, M, A> = Omit<
  VuexStore<S>,
  "getters" | "commit" | "dispatch"
> &
  Getters<G> &
  Commit<M> &
  Dispatch<A>;
