export function group<T, G extends string>(
  list: T[],
  grouper: (item: T) => G
): Map<G, T[]> {
  const groups = new Map<G, T[]>();
  for (const item of list) {
    const group = grouper(item);
    let items = groups.get(group);
    if (!items) {
      items = [];
      groups.set(group, items);
    }
    items.push(item);
  }
  return groups;
}

export function includesAll<T>(list: T[], subList: T[]): boolean {
  for (const item of subList) if (!list.includes(item)) return false;
  return true;
}

export function includesAny<T>(list: T[], otherList: T[]): boolean {
  for (const item of otherList) if (list.includes(item)) return true;
  return false;
}

export function includesValue(
  object: Record<string, unknown>,
  value: unknown
): boolean {
  return Object.values(object).includes(value);
}

export function popRandom<T>(list: T[]): T | undefined {
  if (!list.length) return undefined;
  const index = Math.floor(Math.random() * list.length);
  const [item] = list.splice(index, 1);
  return item;
}

export function remove<T>(list: T[], item: T): boolean {
  const index = list.indexOf(item);
  if (index === -1) return false;
  list.splice(index, 1);
  return true;
}

export function repeat<T>(count: number, func: (index?: number) => T): T[] {
  return [...Array(count)].map((_value, index) => func(index));
}
