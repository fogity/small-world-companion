module.exports = {
  publicPath:
    process.env.NODE_ENV === "production" ? "/small-world-companion/" : "/",
};
